#!/usr/bin/env bash

# exit on error
set -e

echo "Installing poetry..."
curl -sSL https://install.python-poetry.org | python3 -
echo 'export PATH="/home/$USER/.local/bin:$PATH"' >> ~/.bashrc

echo "Installing Python dependencies..."
poetry install

echo "Activate Python virtual environment..."
poetry shell

echo "And install elsa...this might take some time"
git clone https://gitlab.com/tum-ciip/elsa.git
cd elsa
pip install --verbose .
cd -
deactivavte
echo "Wuuhuuu done!!"
echo "Now you can run 'poetry run jupyter lab' and connect to it"
