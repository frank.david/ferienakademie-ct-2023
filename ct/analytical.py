import numpy as np
from ct.XrayOperator import XrayOperator


def create_filter(size, num_angles, filter_type):
    H = np.linspace(-1, 1, size)

    if filter_type == 'ram_lak':
        filter_response = np.abs(H)
    elif filter_type == 'shepp_logan':
        filter_response = np.abs(H) * np.sinc(H / 2)
    elif filter_type == 'cosine':
        filter_response = np.abs(H) * np.cos(H * np.pi / 2)
    elif filter_type == 'none':
        filter_response = np.ones_like(H)
    else:
        raise ValueError(
            "Invalid filter type. Choose from 'ram_lak', 'shepp_logan', or 'cosine'.")

    h = np.tile(filter_response, (num_angles, 1))
    return h


def apply_filtering(sino, h):
    fftsino = np.fft.fft(sino, axis=1)
    projection = np.fft.fftshift(fftsino, axes=0) * np.fft.fftshift(h, axes=1)
    fsino = np.real(np.fft.ifft(np.fft.ifftshift(projection, axes=0), axis=1))
    return fsino


def fbp(sino, vol_size=None, thetas=None, s2c=None, c2d=None, filter_type='ram_lak', projector=None):
    """
    Filtered Back Projection (FBP) algorithm for image reconstruction.

    The FBP algorithm reconstructs an image from its sinogram using a series
    of projection angles and the specified source-to-center and center-to-detector
    distances. This function allows the user to choose the filter for the reconstruction.

    Parameters
    ----------
    sino : numpy array
        The sinogram to be reconstructed.
    thetas : [float]
        Array of projection angles in degrees.
    s2c : float
        Distance from the X-ray source to the center of rotation.
    c2d : float
        Distance from the center of rotation to the detector.
    sino_shape : int, optional
        The number of detector pixels, by default 2500.
    filter_type : str, optional
        The type of filter to use for the reconstruction. Options are 'ram_lak', 'shepp_logan', and 'cosine'.
        By default, 'ram_lak' is used.

    Returns
    -------
    numpy array
        The reconstructed image using the Filtered Back Projection algorithm.
    """
    if vol_size is not None and thetas is not None and s2c is not None and c2d is not None:
        projector = XrayOperator(
            vol_size,
            sino.shape[1:],
            thetas,
            s2c,
            c2d,
        )
    elif projector is None:
        raise InvalidArgumentError(
            'Either specify vol_size, thetas, s2c and c2d or a projector')

    thetas = projector.thetas

    h = create_filter(sino.shape[1], len(thetas), filter_type)
    fsino = apply_filtering(sino, h)

    filtered_backprojection = projector.applyAdjoint(fsino)
    return filtered_backprojection
