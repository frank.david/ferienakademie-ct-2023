import pyelsa as elsa
import numpy as np


def shepp_logan(size: tuple) -> np.ndarray:
    """Create a shepp logan phantom

    Args:
        size (tuple): Size of the phantom (2D or 3D)

    Returns:
        np.ndarray: Shepp-logan phantom of given size
    """

    return elsa.phantoms.modifiedSheppLogan(size)
