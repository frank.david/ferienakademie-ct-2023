import tqdm
import numpy as np

from .functionals import *


def accelerated_proximal_gradient(f, mu, x0, g=Indicator(0, np.inf), n_iters=100, tol=1e-6, callback=None):
    """
    Performs the Fast Proximal Gradient Method (FPGM) using Nesterov's momentum.

    Args:
        f : Differentiable function to minimize
        mu : step length of the gradient step (if f is Lipschitz continuous, this is the Lipschitz constant)
        x0 : Initial guess
        g : Prox friendly function `g`
        n_iters : number of iterations to run the algorithm
        tol : tolerance for early exit
        callback : function to call after each iteration with arguemnts (x, F(x), k), where k is the iteration number, and F = f + g

    Returns:
        x (numpy.ndarray): The point that the algorithm converged to, or the last point in the iteration if the maximum number of iterations was reached.
    """

    x = x0.copy()
    x_old = x0.copy()
    z = x0.copy()
    y = x0.copy()
    t = 1
    t_old = t

    for k in tqdm.trange(n_iters):
        # Gradient step
        z = y - mu * f.grad(x)

        # Proximal step
        x = g.prox(z, mu)

        # Acceleration step
        tmp = (x - x_old)
        y = x + ((t_old - 1) / t) * tmp

        # Just some simple early exit methods
        if np.linalg.norm(tmp) < tol:
            break
        if callback:
            callback(x, (f + g)(x), k)
    return x
