import numpy as np


def power_iteration(A, n_iter=100):
    """
    Power iteration to find the largest singular value of a matrix.
    """
    C = A.T @ A
    b = np.random.rand(C.shape[1])  # initial guess with unit length
    b = b / np.linalg.norm(b)
    for _ in range(n_iter):
        b = C @ b
        b = b / np.linalg.norm(b)
    sigma_1 = np.dot(b, C @ b) / np.linalg.norm(b)  # largest singular value
    return sigma_1
