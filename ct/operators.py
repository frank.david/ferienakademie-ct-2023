import numpy as np

from scipy.sparse.linalg import LinearOperator
import numpy as np


class IdentityOperator(LinearOperator):
    """
    A linear operator that returns the input vector as is.

    Parameters
    ----------
    dims : int
        The dimension of the input vector.

    Attributes
    ----------
    shape : tuple
        The shape of the operator.

    Methods
    -------
    apply(x)
        Returns the input vector as is.
    applyAdjoint(x)
        Returns the input vector as is.
    """

    def __init__(self, inshape, outshape, dtype=np.float32):
        super(IdentityOperator, self).__init__(dtype=dtype,
                                               shape=(np.prod(inshape), np.prod(outshape)))

    def apply(self, x):
        return x

    def applyAdjoint(self, x):
        return x

    def _matvec(self, x):
        """Perform the forward projection, implement the scipy.LinearOperator interface"""
        return x

    def _adjoint(self):
        return self


def forward_diff(x, axis=-1, sampling=1):
    """Apply forward differences to any chosen axis of the given input array
    Notes
    -----
    For simplicity, given a one dimensional array, the first-order forward
    stencil is:
    .. math::
    y[i] = (x[i+1] - x[i]) / \Delta x
    """
    # swap the axis we want to calculate finite differences on,
    # to the last dimension
    x = x.swapaxes(axis, -1)
    # create output vector
    y = np.zeros_like(x)
    # compute finite differences
    y[..., :-1] = (x[..., 1:] - x[..., :-1]) / sampling
    # swap axis back to original position
    return y.swapaxes(axis, -1)


def adjoint_forward_diff(x, axis=-1):
    """Apply the adjoint of the forward differences to any chosen axis of
    the given input array
    """
    x = x.swapaxes(axis, -1)
    y = np.zeros_like(x)
    y[..., :-1] -= x[..., :-1]
    y[..., 1:] += x[..., :-1]
    return y.swapaxes(axis, -1)


class FirstDerivative(LinearOperator):
    """Operator which applies finite differences to the given dimensions
    Notes
    -----
    This operator computed the finite differences lazily, i.e. doesn't construct
    a matrix and consumes unnecessary memory
    """

    def __init__(self, dims, inshape, dtype=np.float32):
        super(FirstDerivative, self).__init__(dtype=dtype,
                                              shape=(np.prod((dims,) + inshape), np.prod(inshape)))
        self.dims = dims
        self.inshape = inshape

    def apply(self, x):
        x = x.reshape(self.inshape)
        grad = np.zeros((self.dims, ) + x.shape)
        for i in range(self.dims):
            grad[i, ...] = forward_diff(x, axis=i).copy()
        return grad

    def applyAdjoint(self, x):
        x = x.reshape((self.dims,) + self.inshape)
        grad = np.zeros(x.shape[1:])
        for i in range(x.shape[0]):
            grad[...] += adjoint_forward_diff(x[i], axis=i).copy()
        return grad

    def _matvec(self, x):
        """Perform the forward projection, implement the scipy.LinearOperator interface"""
        return self.apply(x).flatten("C")

    def _adjoint(self):
        """Return the adjoint, implement the scipy.LinearOperator interface"""

        class AdjointOperator(LinearOperator):
            def __init__(self, forward):
                self.forward = forward
                super().__init__(self.forward.dtype, np.flip(self.forward.shape))

            def _matvec(self, y):
                return self.forward.applyAdjoint(y).flatten("C")

            def _adjoint(self):
                return self.forward

        return AdjointOperator(self)


class StackedOperator:
    """Stacked operator.

    The stacked operator is defined as K = [K_1, ..., K_n]^T, where K_i is some linear operator.

    The operator can be applied to a vector, which is a list n of arrays. See the implementation.
    Importantly, the input is a list of n arrays, the output is also a list of n arrays.
    Each operator K_i is applied to the i-th array in the list.

    The adjoint is defined as sum(K_i^T * x_i)
    """

    def __init__(self, operators):
        self.ops = operators

    def apply(self, x):
        l = np.array([], dtype=object)
        l.resize(len(self.ops))
        for i, op in enumerate(self.ops):
            l[i] = op.apply(x)
        return l

    def applyAdjoint(self, y):  # now y is a list
        x = self.ops[0].applyAdjoint(y[0])
        for yi, op in zip(y[1:], self.ops[1:]):
            x += op.applyAdjoint(yi)
        return x

    def __getitem__(self, index):
        return self.ops[index]

    def _matvec(self, x):
        """Perform the forward projection, implement the scipy.LinearOperator interface"""
        return self.apply(x).flatten("C")

    def _adjoint(self):
        """Return the adjoint, implement the scipy.LinearOperator interface"""

        class AdjointOperator(LinearOperator):
            def __init__(self, forward):
                self.forward = forward
                super().__init__(self.forward.dtype, np.flip(self.forward.shape))

            def _matvec(self, sino):
                return self.forward.applyAdjoint(sino).flatten("C")

            def _adjoint(self):
                return self.forward

        return AdjointOperator(self)
