import numpy as np
from abc import ABC, abstractmethod
from scipy.special import kl_div, xlogy


class Functional(ABC):
    """Base class for functions which map from R^N to R (i.e. functionals)

    A Functional must implement the call operator `__call__`. It may also overload
    `grad`, `prox`, `conjugate` and `proxdual`. `proxdual` provides a default
    implementation using the Moreau's identity, it used `prox` (which needs to be implemented obviously)

    Further, the base class provides useful overloads for addition of functionals
    and the multiplication of a functional with a scalar.
    """
    @abstractmethod
    def __call__(self, x):
        """Evaluate the functional at x"""
        pass

    def grad(self, x):
        """Evaluate the gradient of the functional at x"""
        raise RuntimeError("Functional is not differentiable")

    def prox(self, x, sigma):
        """Evaluate the proximal operator of the functional at x with given sigma"""
        raise RuntimeError("Functional is not proxiable")

    def conjugate(self, x):
        """Evaluate the conjugate of the functional at x"""
        raise RuntimeError("Functional conjugate is not implemented")

    def proxdual(self, x, sigma):
        """Evaluate the proximal of the convex conjugate of the functional

        Default implementation of the proxdual using Moreau's identity
        """
        try:
            return x - sigma * self.prox(x / sigma, 1 / sigma)
        except RuntimeError as e:
            raise RuntimeError(
                "Can't evaluate proximal of conjugate of functional")

    def __add__(self, other):
        if isinstance(other, Functional):
            return FunctionalSum(self, other)
        raise RuntimeError("Can only add two Functional objects")

    def __mul__(self, rhs):
        if np.isscalar(rhs):
            return FunctionalScalarMul(self, rhs)
        raise RuntimeError("Functional can only be multiplied by a scalar")

    def __rmul__(self, lhs):
        if np.isscalar(lhs):
            return FunctionalScalarMul(self, lhs)
        raise RuntimeError("Functional can only be multiplied by a scalar")


class FunctionalSum(Functional):
    """Represent a sum of two functionals

    Attributes
    ----------
    lhs : left hand side functional
    rhs : right hand side functional
    """

    def __init__(self, lhs, rhs):
        """Initialize a sum of two functionals"""
        self.lhs = lhs
        self.rhs = rhs

    def __call__(self, x):
        """The call operator for the sum of two functionals"""
        return self.lhs(x) + self.rhs(x)

    def grad(self, x):
        """The gradient of the sum of two functionals"""
        return self.lhs.grad(x) + self.rhs.grad(x)


class FunctionalScalarMul(Functional):
    """The product of a functional with a scalar

    Attributes
    ----------
    f : the function of the product
    scalar : the scalar of the product
    """

    def __init__(self, f, scalar):
        self.f = f
        self.scalar = scalar

    def __call__(self, x):
        """Evaluate the product of a functional with a scalar"""
        return self.f(x) * self.scalar

    def conjugate(self, x):
        """Evaluate the convex conjugate as `s * f(x / s)`"""
        return self.scalar * self.f.conjugate(x / self.scalar)

    def grad(self, x):
        """Evaluate the gradient of the product of a functional with a scalar"""
        return self.scalar * self.f.grad(x)

    def prox(self, x, sigma):
        """Evaluate the proximal operator of the product of a functional with a scalar"""
        return self.f.prox(x, sigma * self.scalar)

    def proxdual(self, x, sigma):
        """Evaluate the proximal of the convex conjugate of the product of a functional with a scalar

        The proxdual of the product of a functional with a scalar is given by
        :math:`x - \sigma \operatorname{prox}_{f \alpha^{-1} \tau}(\alpha^{-1} x)`
        """
        return x - sigma * self.f.proxdual(x / sigma, self.scalar / sigma)


class LeastSquares(Functional):
    """Least squares problem formulation: 0.5 * ||Ax - b||^2_2

    Attributes
    ----------
    A : the forward model for the least squares problem
    b : the measurement data
    """

    def __init__(self, A, b):
        self.A = A
        self.b = b

    def __call__(self, x):
        return 0.5 * np.linalg.norm(self.A.apply(x) - self.b)**2

    def grad(self, x):
        return self.A.applyAdjoint(self.A.apply(x) - self.b)


class WeightedLeastSquares(Functional):
    """Weighted Least squares problem formulation: 0.5 * ||Ax - b||^2_W

    Attributes
    ----------
    A : the forward model for the least squares problem
    b : the measurement data
    W : the weights, should ideally be the reciprocal of the variance of the measurements

    Notes
    -----

    The weightes least squares formulations is useful in the low-dose setting, where
    the assumption of Gaussian noise is not valid anymore. Using the transmission data
    and a flat field image the reciprocal of the variance can be approximated easily.
    """

    def __init__(self, A, b, W):
        self.A = A
        self.b = b
        self.W = W

    def __call__(self, x):
        return 0.5 * np.linalg.norm(self.W * (self.A.apply(x) - self.b))**2

    def grad(self, x):
        return self.A.applyAdjoint(self.W * (self.A.apply(x) - self.b))


class TransmissionLogLikelihood(Functional):
    """Transmission log-likelihood formulation

    The transmission log-likelihood is given by
    :math:`log(b * exp(-Ax) + r) - y * log(b * exp(-Ax) + r)`

    Attributes
    ----------
         A: linear operator to apply to x
         y: the measurement data vector (not log transformed)
         b: the blank scan data vector

    Notes
    -----

    Similarly to the weighted least squares formulations, the transmission log-likelihood
    is useful, if the assumption of Gaussian noise is not valid, but a Poisson distribution
    is assumed. The formulation can be derived from the Poisson distribution
    :math:`\frac{\lambda^k e^{-\lambda}}{k!}`. This is also equivalent to optimizing
    the Kullback-Leibler divergence.
    """

    def __init__(self, A, y, b):
        self.A = A
        self.y = y
        self.b = b

    def __call__(self, x):
        Ax = self.A.apply(x)
        return np.sum(self.b * np.exp(-Ax) - self.y * np.log(self.b * np.exp(-Ax)))

    def grad(self, x):
        Ax = self.A.apply(x)
        return self.A.applyAdjoint(self.y - (self.b * np.exp(-Ax)))


class Indicator(Functional):
    """Indicator function for box constraints

    Attributes
    ----------
    low : the lower bound of the box constraints
    high : the upper bound of the box constraints

    Notes
    -----

    The projection onto the convex set :math:`C` is usually easy to compute. A specifically
    interesting example is the non-negativity set and box set. These occure quite
    commonly in X-ray CT and are easily implemented.
    """

    def __init__(self, low=-np.inf, high=np.inf):
        self.low = low
        self.high = high

    def __call__(self, x):
        return 0 if np.all(x > self.low) and np.all(x < self.high) else np.inf

    def prox(self, x, _):
        return np.clip(x, self.low, self.high)


class L2Squared(Functional):
    """L2 Squared function :math:`||x||_2^2`"""

    def __call__(self, x):
        return np.dot(x, x)

    def prox(self, x, sigma):
        return x / (1 + sigma)


class L1(Functional):
    """L1 function :math:`||x||_1`

    Notes
    -----

    The L1 functional is useful in many different optimization formulations, such
    as LASSO, or anisotropic TV regularization. The L1 norm is a sparsifying operations,
    and together with e.g. finite differences and approximate a sparse gradient.
    """

    def __call__(self, x):
        return np.sum(np.abs(x))

    def prox(self, x, sigma):
        return np.sign(x) * np.maximum(np.abs(x) - sigma, 0)

    def proxdual(self, x, sigma):
        return np.clip(x, -sigma, sigma)


class L21(Functional):
    """L21 function :math:`||x||_{2,1}`

    Notes
    -----

    The L21 functional assumes the input `x` to be a matrix. It computes
    the the sum of the square root of the column sums. I.e. if `x` is of size n by m.
    Then first it computes the column sums, which results in a vector of size m.
    Then the square root is applied, and finally that vector is summed.

    This functionals is most often used to regularize isotropic TV. This form of
    TV usually doesn't penalises rotational invariants compared to anisotropic TV.

    For further readings check out https://epubs.siam.org/doi/10.1137/16M1075247
    """

    def __call__(self, x):
        return np.sum(np.sqrt(np.sum(np.square(x), axis=0)))

    def prox(self, x, sigma):
        norm = np.linalg.norm(x, axis=0)
        coeff = np.maximum(0, 1 - sigma / np.maximum(norm, sigma))
        return coeff * x


class SeparableSum(Functional):
    """ Separable sum of functions

    Notes
    -----
    This function is partially interesting if you want to apply multiple regularization
    terms in a single function `f`. Then you can kind of stack them (using the StackedOperator)
    and then the proximal is still easily computed.
    """

    def __init__(self, fns):
        """
        Args:
            fns (list): List of functions that are summed
        """
        self.fns = fns

    def __call__(self, x: np.ndarray) -> np.ndarray:
        return np.sum([f(x) for f in self.fns])

    def conjugate(self, x):
        return np.sum([f.conjugate(x) for f in self.fns])

    def grad(self, x):
        out = np.zeros_like(x)
        for i, fn in enumerate(self.fns):
            out[i] = fn.gradient(x[i])

    def prox(self, x, sigma):
        out = np.zeros_like(x)
        for i, fn in enumerate(self.fns):
            out[i] = fn.prox(x[i], sigma)
        return out

    def proxdual(self, x, sigma):
        out = np.zeros_like(x)
        for i, fn in enumerate(self.fns):
            out[i] = fn.proxdual(x[i], sigma)
        return out
