ndet_pixels = 200 # number of detector pixels
thetas = np.linspace(0, 360, 360, endpoint=False) # angles in degree [0, 360)
s2c = 1000 # distance from _source_ to _center_
c2d = 200  # distance from _center_ to _detector_

# Create a circular trajectory with radius s2c + c2d, the source is typically moved further back
# to ensure good coverage of the deteector and more parallel like beam
A = ct.XrayOperator(volume_size, [ndet_pixels], thetas, s2c, c2d)

# Compute forward projection / simulate measurement
sinogram = A.apply(phantom)
