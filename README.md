# Low-dose challenge Ferienakademie 2023

This code is intented to be used for course 10 of the [Ferienakademie
2023](https://ferienakademie.de/en/courses-2023/). The instructions have been
written with that in mind and may not be useful outsite of that. Feel free to
use the code in any way you like.

The code provides a simple reconstruction framework for attenuation X-ray
Computed Tomography (CT). It implements a 2D FBP and FISTA algorithm.

In any case, nice to see you checked out this project! Let's get right to the
setup.

## Code Structure

You can find most of the implementation regarding algorithms and similar in the
`ct` folder. All images, sinograms and similar can be found in the `data`
folder. In the `notebook` folder, you can find Jupyter notebooks you can work
through. It tries to walk you through basic forward and backward projections,
basic reconstructions and why analytical reconstructions can be insufficient
for low-dose data.

## Setup

The easy way is to SSH into our server and use something like VSCode or similar
to edit the files remotly. We really highly recommend this. This will get you
started way way quicker! Further, either use Linux or MacOS. If you must use
Windows, then either use WSL or ssh into our server again using something like
VSCode (preferably Vim or Emacs, but at this point :-P).

You have been warned!

Alright, so let's set everything up. First, install
[poetry](https://python-poetry.org/). This is a creat wrapper and dependency manager
around pip. With that our of the way, from the project root run:

```bash
poetry install
```

Now activate the poetry environment with

```bash
poetry shell
```

Now, clone _elsa_, one of the commands is obviously enough:

```bash
# Using SSH
git clone git@gitlab.com:tum-ciip/elsa.git

# Using HTTPS
git clone https://gitlab.com/tum-ciip/elsa.git
```

Enter the _elsa_ directory and run (be sure to be in the poetry environment):

```bash
pip install --verbose .
```

Check the install requieremnts for elsa
[here](https://gitlab.com/tum-ciip/elsa#requirements). We also have a small
[troubleshooting](https://gitlab.com/tum-ciip/elsa#requirements) area in the
_elsa_ repo.

With that out of the way, you should be able to run the notebooks on your local
machine.
